// SPDX-License-Identifier: GPL-3.0-or-later
// (C) Copyright 2024 Kunal Mehta <legoktm@debian.org>
use anyhow::{Context, Result};
use reqwest::header::{HeaderMap, HeaderValue, AUTHORIZATION};
use reqwest::Client;
use serde::Deserialize;
use std::fs;
use std::time::Duration;
use tokio::time::{interval, MissedTickBehavior};

#[derive(Debug, Deserialize)]
struct Config {
    mastodon: MastodonConfig,
}

#[derive(Debug, Deserialize)]
struct MastodonConfig {
    instance_url: String,
    access_token: String,
    keywords_to_report: Vec<String>,
    user_agent: String,
}

#[derive(Debug, Deserialize)]
struct Post {
    id: String,
    in_reply_to_id: Option<String>,
    content: String,
    account: Account,
}

#[derive(Debug, Deserialize)]
struct Account {
    id: String,
    acct: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    // Load configuration from TOML file
    let config_str = fs::read_to_string("config.toml")
        .context("Failed to read config.toml")?;
    let config: Config =
        toml::from_str(&config_str).context("Failed to parse config.toml")?;

    let client = Client::builder()
        .user_agent(&config.mastodon.user_agent)
        .build()?;

    println!("Hello, here are the bad words I'm watching for:");
    dbg!(&config.mastodon.keywords_to_report);
    // Parse Mastodon configuration

    // Keep track of most recently seen message ID
    let mut last_seen_id: Option<String> = None;

    // Set up authorization header
    let mut headers = HeaderMap::new();
    headers.insert(
        AUTHORIZATION,
        HeaderValue::from_str(&format!(
            "Bearer {}",
            &config.mastodon.access_token
        ))?,
    );

    let mut interval = interval(Duration::from_secs(30));
    interval.set_missed_tick_behavior(MissedTickBehavior::Delay);
    // Fetch Mastodon timeline and report posts containing the keyword
    loop {
        match fetch_mastodon_timeline(
            &client,
            &config.mastodon.instance_url,
            &headers,
            &last_seen_id,
        )
        .await
        {
            Ok(posts) => {
                for post in posts.iter().rev() {
                    if post.in_reply_to_id.is_none()
                        && config
                            .mastodon
                            .keywords_to_report
                            .iter()
                            .any(|word| post.content.contains(word))
                    {
                        println!(
                            "I'm reporting {} status {} because it said: {}",
                            post.account.acct, post.id, post.content
                        );

                        report_post(
                            &client,
                            &config.mastodon.instance_url,
                            &headers,
                            &post.account.id,
                            &post.id,
                        )
                        .await?;
                    }
                    last_seen_id = Some(post.id.to_string());
                }
            }
            Err(e) => eprintln!("Error fetching Mastodon timeline: {}", e),
        }
        // Poll every 30s
        interval.tick().await;
    }
}

async fn fetch_mastodon_timeline(
    client: &Client,
    instance_url: &str,
    headers: &HeaderMap,
    last_seen_id: &Option<String>,
) -> Result<Vec<Post>> {
    let mut url = format!(
        "{}/api/v1/timelines/public?limit=40&remote=true",
        instance_url
    );
    if let Some(last_id) = last_seen_id {
        url.push_str(&format!("&since_id={}", last_id));
    }
    let response = client.get(&url).headers(headers.clone()).send().await?;
    let timeline: Vec<Post> = response.json().await?;
    println!("Fetched {} posts.", timeline.len());
    Ok(timeline)
}

async fn report_post(
    client: &Client,
    instance_url: &str,
    headers: &HeaderMap,
    account_id: &str,
    post_id: &str,
) -> Result<()> {
    let url = format!("{}/api/v1/reports", instance_url);
    let params = [
        ("account_id", account_id),
        ("status_ids[]", post_id),
        ("category", "spam"),
        ("comment", "Automated report, please verify manually."),
    ];
    let resp = client
        .post(&url)
        .headers(headers.clone())
        .form(&params)
        .send()
        .await?;
    println!(
        "status_code={}, text={}",
        resp.status().as_u16(),
        resp.text().await?
    );
    Ok(())
}
