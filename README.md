## masto-monitor

Rudimentary anti-spam bot that watches the federated timeline for
specific keywords and sends automated reports for them.

Copy config.toml.example to config.toml and fill in the specified fields.

More instructions to come soon.

## Reuse

Licensed under the GPL v3, or any later version. The initial scaffolding was
written by [ChatGPT 3.5](https://chat.openai.com/share/5b220e4f-e660-456e-a658-9f91d472f8ea) and presumably, public domain.
